# threejs-webpack-template

This is a basic Template for using [three.js](https://threejs.org/) with [typescript](https://www.typescriptlang.org/) and [webpack](https://webpack.js.org/).

The code in this Template is based on [this](https://www.youtube.com/watch?v=PPwR7h5SnOE) tutorial from the Youtube channel [SimonDev](https://www.youtube.com/channel/UCEwhtpXrg5MmwlH04ANpL8A).

I made this primarily as a template for myself, i will make it public so other people who want to use typescript with three.js as well have an easier start.

Just as SimonDev says in his Video you can use his example code.

This applies to this project as well just fork/use/improve it :)

The used hdri for the skybox is [this one](https://hdrihaven.com/hdri/?c=skies&h=sunflowers) from [HDRIHaven](https://hdrihaven.com/).

Used [this](https://matheowis.github.io/HDRI-to-CubeMap/) web based converter to convert it into a cubemap.